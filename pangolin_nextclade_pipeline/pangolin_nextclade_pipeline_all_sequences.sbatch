#!/bin/bash

# exit when any command fails
set -eo pipefail

timestamp() {
  date "+%Y-%m-%d %H:%M:%S"
}

DATE="$(date +'%Y_%m_%d_%H_%M')"
MODE="all"
SLURM_ACCOUNT="emergen_ifb"
OUT_DIR="/shared/projects/emergen_ifb/nextclade_pangolin_working_dir/nextclade_pangolin_${MODE}_${DATE}/out"
WORKING_DIR="/shared/projects/emergen_ifb/nextclade_pangolin_working_dir/nextclade_pangolin_${MODE}_${DATE}"
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
EMERGEN_DB_API_TOKEN=${EMERGEN_DB_API_TOKEN}
mkdir -p "${WORKING_DIR}" "${OUT_DIR}"

echo "-------------------"
echo "Variables set:"
echo "OUT_DIR=$OUT_DIR"
echo "WORKING_DIR=$WORKING_DIR"
echo "SCRIPTPATH=$SCRIPTPATH"
echo "MODE=$MODE"
echo "DATE=$DATE"
echo "-------------------"

DOWNLOAD_ALL_SEQUENCES=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/download_all_sequences_emergendb_%j.out --job-name=dl_sequences_emergendb --parsable --export=DATE="${DATE}",MODE="${MODE}",EMERGEN_DB_API_TOKEN="${EMERGEN_DB_API_TOKEN}" "${SCRIPTPATH}"/download_all_sequences_emergendb.sbatch)
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${DOWNLOAD_ALL_SEQUENCES} (downloading all sequences from EMERGENDB)"

SPLIT_FASTA_FILE=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/split_fasta_file_%j.out --kill-on-invalid-dep=yes --parsable --export=DATE="${DATE}",MODE="${MODE}",SCRIPTPATH="${SCRIPTPATH}" "${SCRIPTPATH}"/split_fasta_file.sbatch)
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${SPLIT_FASTA_FILE} (splitting fasta file containing all sequences)"

TMP_FILE_COUNT=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/tmp_file_count_%j.out --kill-on-invalid-dep=yes --parsable --export=DATE="${DATE}",MODE="${MODE}",SCRIPTPATH="${SCRIPTPATH}" "${SCRIPTPATH}"/tmp_fasta_files_count.sbatch)
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${TMP_FILE_COUNT} (counting split fasta files)"

DOWNLOAD_NEXTCLADE_REFERENCES=$(sbatch -A ${SLURM_ACCOUNT} -o "${OUT_DIR}"/download_nextclade_reference_files_%j.out --job-name=dl_nextclade_reference_files --kill-on-invalid-dep=yes --parsable --dependency=afterany:"${TMP_FILE_COUNT}" --export=DATE="${DATE}",MODE=${MODE} "${SCRIPTPATH}"/download_nextclade_reference_files.sbatch)
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${DOWNLOAD_NEXTCLADE_REFERENCES} (downloading Nextclade reference files)"

RUN_NEXTCLADE=$(sbatch -A ${SLURM_ACCOUNT} -o "${OUT_DIR}"/nextclade_run_%A_%a.out --kill-on-invalid-dep=yes --parsable --dependency=afterok:"${DOWNLOAD_NEXTCLADE_REFERENCES}" --export=DATE="${DATE}",MODE="${MODE}" --array=0-"$(cat ${WORKING_DIR}/tmp/file_count.txt)" "${SCRIPTPATH}"/run_nextclade.sbatch)  # Number of fasta files in tmp dir used to set number of job in the array
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job array ${RUN_NEXTCLADE} (running Nextclade)"

UPDATE_PANGO_DATA=$(sbatch -A ${SLURM_ACCOUNT} -o "${OUT_DIR}"/update_pango_data_%a.out --kill-on-invalid-dep=yes --parsable --dependency=afterok:"${SPLIT_FASTA_FILE}" "${SCRIPTPATH}"/update_pango_data.sbatch)
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${UPDATE_PANGO_DATA} (update Pangolin data)"

RUN_PANGOLIN=$(sbatch -A ${SLURM_ACCOUNT} -o "${OUT_DIR}"/pangolin_run_%A_%a.out --kill-on-invalid-dep=yes --parsable --dependency=afterok:"${UPDATE_PANGO_DATA}" --export=DATE="${DATE}",MODE="${MODE}" --array=0-"$(cat ${WORKING_DIR}/tmp/file_count.txt)" "${SCRIPTPATH}"/run_pangolin.sbatch)  # Number of fasta files in tmp dir used to set number of job in the array
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job array ${RUN_PANGOLIN} (running Pangolin)"

# MERGE_NEXTCLADE_RESULTS=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/merge_nextclade_results_%j.out --job-name=merge_nextclade_results --kill-on-invalid-dep=yes --parsable --dependency=afterany:"${RUN_NEXTCLADE}" --export=DATE="${DATE}",MODE="${MODE}" "${SCRIPTPATH}"/merge_nextclade_results.sbatch)
# echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${MERGE_NEXTCLADE_RESULTS} (merging Nextclade results)"

# MERGE_PANGOLIN_RESULTS=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/merge_pangolin_results_%j.out --job-name=merge_pangolin_results --kill-on-invalid-dep=yes --parsable --dependency=afterany:"${RUN_PANGOLIN}" --export=DATE="${DATE}",MODE="${MODE}" "${SCRIPTPATH}"/merge_pangolin_results.sbatch)
# echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${MERGE_PANGOLIN_RESULTS} (merging Pangolin results)"

TMP_FILE_COUNT_VAR=$(cat "${WORKING_DIR}/tmp/file_count.txt")
echo "TMP_FILE_COUNT_VAR=$TMP_FILE_COUNT_VAR"

UPLOAD_RESULTS=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/upload_pangolin_results_%j_out --job-name=upload_results_emergendb --kill-on-invalid-dep=yes --parsable --dependency=afterany:"${RUN_PANGOLIN}" --export=DATE="${DATE}",MODE="${MODE}",EMERGEN_DB_API_TOKEN="${EMERGEN_DB_API_TOKEN}",TMP_FILE_COUNT_VAR="${TMP_FILE_COUNT_VAR}" "${SCRIPTPATH}"/upload_pangolin_nextclade_results_emergendb.sbatch)
echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${UPLOAD_RESULTS} (uploading Pangolin & Nextclade results to EMERGENDB)"

# CLEANUP=$(sbatch -A ${SLURM_ACCOUNT} -W -o "${OUT_DIR}"/cleanup_%j.out --job-name=cleanup --kill-on-invalid-dep=yes --dependency=afterany:"${UPLOAD_RESULTS}":"${MERGE_NEXTCLADE_RESULTS}" --export=DATE="${DATE}",MODE="${MODE}" "${SCRIPTPATH}"/cleanup.sbatch)
# echo "$(timestamp) [pangolin_nextclade_pipeline_all_sequences] Submitted job ${CLEANUP} (clean up working dir)"
