#!/usr/bin/python3

import sys


def get_different_columns(prev, new):
    prev_cols = []
    new_cols = []

    prev_not_new = []
    new_not_prev = []

    with open(prev, "r") as prev_handle:
        for line in prev_handle:
            prev_cols.append(line.rstrip())

    with open(new, "r") as new_handle:
        for line in new_handle:
            new_cols.append(line.rstrip())

    for x in prev_cols:
        if x not in new_cols:
            prev_not_new.append(x)

    for x in new_cols:
        if x not in prev_cols:
            new_not_prev.append(x)

    print("Removed/renamed columns")
    print("\t".join([x for x in new_not_prev + prev_not_new]))
    print("----")
    print("New columns")
    print("\t".join(new_not_prev))
    print("----")
    print("Old columns")
    print("\t".join(prev_not_new))


if __name__ == "__main__":
    previous_version = sys.argv[1]
    new_version = sys.argv[2]

    get_different_columns(prev=previous_version, new=new_version)
