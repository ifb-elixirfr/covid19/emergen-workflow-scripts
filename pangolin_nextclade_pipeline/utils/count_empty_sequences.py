import sys

empty_sequences = 0

with open(sys.argv[1], "r") as fhandle:
    prev_line = ""
    for line in fhandle:
        if line.startswith(">"):
            if prev_line.startswith(">"):
                empty_sequences += 1
        prev_line = line

print(f"Empty sequences in file {sys.argv[1]}: {empty_sequences}")
