# pangolin_nextclade_pipeline

Scripts to run [Pangolin](https://cov-lineages.org/resources/pangolin/usage.html) 
and [Nextclade](https://docs.nextstrain.org/projects/nextclade/en/latest/user/nextclade-web.html)
command line tools to analyse/re-analyse sequences hosted on EMERGENDB (lineage and clade assignation)
and upload the results to EMERGENDB.

---
### Usage
  * Analyse all sequences from EMERGENDB:

    `sh pangolin_nextclade_pipeline_all_sequences.sh <emergen_db_api_token>`


  * Analyse sequences not yet analysed by Pangolin and Nextclade:
  
    `sh pangolin_nextclade_pipeline_partial_sequences.sh <emergen_db_api_token>`


To manually upload the results, use the script [manual_upload_pangolin_nextclade_results_emergendb](manual_upload_pangolin_nextclade_results_emergendb.sh)
, with the arguments `manual_upload_pangolin_nextclade_results_emergendb $MODE $DATE` where `$MODE` is the pipeline you used
(either "partial" or "all") and `$DATE` is the date of the pipeline call (format `YYYY_mm_dd_HH_MM`). Both of these 
are displayed in the name of the directory created by the pipeline run.

---
### Workflow 
  1) Download sequences from EMERGENDB
  2) Download nextclade reference files
  3) Split the sequences fasta files into smaller fasta files
  4) Run Pangolin and Nextclade on these smaller fasta datasets
  5) Merge Pangolin and Nextclade results
  6) Upload results files to EMERGENDB
  
---
Requirements:
```
biopython==1.79
numpy==1.21.3
```