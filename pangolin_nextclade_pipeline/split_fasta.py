#!/usr/bin/python3

import os
import logging
import argparse

from datetime import date
from Bio import SeqIO

# Create a logger and set minimal level to debug
logger = logging.getLogger("emergen_pangolin_nextclade_pipeline")
logger.setLevel(logging.DEBUG)

# Create a console handler and set minimal level to debug
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# Format for the logging message
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# Set the format for the console handler
console_handler.setFormatter(formatter)
# Affect console handler to the logger
logger.addHandler(console_handler)


def split_all_sequences_fasta_file(input_fasta, output_dir):
    """
    Split an input fasta file into multiple smaller files
    The parameter 'records_per_file' controls how large these files will be
    Resulting fasta files are located in the specified output_dir and are named emergendb_sequences_dd_mm_YYY_ID.fa

    :param input_fasta:
    :param output_dir:
    :return:
    """

    records = []
    for record in SeqIO.parse(input_fasta, "fasta"):
        records.append(record)
    logger.info(
        "Number of sequences in {fasta_file}: {records}".format(
            fasta_file=input_fasta, records=len(records)
        )
    )

    records_per_file = 1000

    # Init file id (appended to every output filename)
    file_id = 0
    # Convert the input records list into a list of lists (chunks), each list containing "records_per_file" sequences
    # Basically does [record1, record2, ...] -> [[record1, record2], [record3, record3], ...]
    # The last chunk can be smaller than the rest of the chunks as the generator reaches the end of the records list
    chunks = [
        records[x : x + records_per_file]
        for x in range(0, len(records), records_per_file)
    ]
    for chunk in chunks:  # Iterate over these lists
        output_fasta_file = os.path.join(
            os.path.abspath(output_dir),
            "emergendb_sequences_{date}_{id}.fa".format(
                date=date.today().strftime("%Y_%m_%d_%H_%M"), id=file_id
            ),
        )
        with open(output_fasta_file, "w") as output_handle:
            logger.info(
                "Writing {nb_records} sequences in file {outfile}".format(
                    nb_records=records_per_file, outfile=output_fasta_file
                )
            )
            for record in chunk:
                SeqIO.write(record, output_handle, "fasta")
        file_id += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    # Add arguments to parser
    parser.add_argument(
        "--outdir", type=str, help="Output directory for the resulting fasta files"
    )
    parser.add_argument("--input-fasta", type=str, help="Input fasta file to be split")
    args = parser.parse_args()

    split_all_sequences_fasta_file(input_fasta=args.input_fasta, output_dir=args.outdir)
