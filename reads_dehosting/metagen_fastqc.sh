#!/bin/bash
#SBATCH --mem 8G
#SBATCH --cpus-per-task 8

# exit when any command fails
set -e
set -o pipefail

usage()
{
cat << EOF
usage: $0 options

Clean and remove host DNA sequences from FASTQ files.

OPTIONS:
   -h      Show help message
   -t      Number of threads (recommended: 8) [REQUIRED]
   -f      Forward or single-end fastq file (*.fastq.gz or *_1.fastq.gz) [REQUIRED]
   -c      BWA-indexed host genome (.fa) [REQUIRED]
   -r	   Reverse fastq file (*_2.fastq.gz) [OPTIONAL]
EOF
}

THREADS=
FASTQ_R1=
FASTQ_R2=
REF=

module purge

module load samtools/1.9
module load bwa/0.7.17
module load fastqc/0.11.9
module load trim-galore/0.6.5
module load bedtools/2.30.0

while getopts "ht:r:f:c:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      THREADS=${OPTARG}
      ;;
    f)
      FASTQ_R1=$OPTARG
      ;;
    r)
      FASTQ_R2=$OPTARG
      ;;
    c)
      REF=$OPTARG
      ;;
    ?)
      usage
      exit
      ;;
  esac
done

timestamp() {
  date +"%H:%M:%S"
}

echo "$(timestamp) [ metagen-fastqc ] Parsing command-line"
# Check if all required arguments are supplied
if [[ -z ${THREADS} ]] || [[ -z ${FASTQ_R1} ]] || [[ -z ${REF} ]]
then
     echo "ERROR : Please supply required arguments"
     usage
     exit 1
fi

if [ ${THREADS} -eq 1 ]
then
    THREADS_SAM=1
else
    THREADS_SAM=$(($THREADS-1))
fi

tmp_dir="$(dirname ${FASTQ_R1})/decontamination_tmp"
final_result_dir="$(dirname ${FASTQ_R1})/fastq_clean"
tags_dir="$(dirname ${FASTQ_R1})"

if [[ ! -z ${FASTQ_R2} ]]
then
  echo "$(timestamp) [ metagen-fastqc ] Cleaning FASTQ files (paired-end mode)"
  trim_galore --paired "${FASTQ_R1}" "${FASTQ_R2}" -o "$tmp_dir"
  name=${FASTQ_R1%%_R1*}

  echo "$(timestamp) [ metagen-fastqc ] Mapping files to host genome: ${REF}"
  bwa mem -M -t ${THREADS} ${REF} "$tmp_dir/${name}_R1_val_1.fq.gz" "$tmp_dir/${name}_R2_val_2.fq.gz" | samtools view -@ ${THREADS_SAM} -f 12 -F 256 -uS - -o "$tmp_dir"/${name}_both_unmapped.bam
	samtools sort -@ ${THREADS_SAM} -n "$tmp_dir/${name}_both_unmapped.bam" -o "$tmp_dir/${name}_both_unmapped_sorted.bam"
	bedtools bamtofastq -i "$tmp_dir/${name}_both_unmapped_sorted.bam" -fq "$final_result_dir/${name}_R1_clean.fastq" -fq2 "$tmp_dir/${name}_R2_clean.fastq"

	echo "$(timestamp) [ metagen-fastqc ] Compressing output files"
	gzip "$final_result_dir/${name}_R1_clean.fastq"
	gzip "$final_result_dir/${name}_R2_clean.fastq"
	touch "$tags_dir/${FASTQ_R1}.clean" "$tags_dir/${FASTQ_R2}.clean"

  echo "$(timestamp) [ metagen-fastqc ] Cleaning tmp files"
  rm -f ${name}*trimming* ${name}_both_unmapped.bam ${name}_both_unmapped_sorted.bam ${name}_*_trimmed.fq.gz ${name}_*_val_*.fq.gz ${name}_*.fastq.gz_*txt
  rm -rf "$tmp_dir"

  FILTERED_READS_R1_BEFORE=$(zcat "${FASTQ_R1}" | echo $((`wc -l`/4)))
  FILTERED_READS_R1_AFTER=$(zcat "${name}_R1_clean.fastq.gz" | echo $((`wc -l`/4)))
  FILTERED_READS_R1_DIFF=$((${FILTERED_READS_R1_BEFORE}-${FILTERED_READS_R1_AFTER}))

  FILTERED_READS_R2_BEFORE=$(zcat "${FASTQ_R2}" | echo $((`wc -l`/4)))
  FILTERED_READS_R2_AFTER=$(zcat "${name}_R2_clean.fastq.gz" | echo $((`wc -l`/4)))
  FILTERED_READS_R2_DIFF=$(($FILTERED_READS_R2_BEFORE-$FILTERED_READS_R2_AFTER))

  echo "$(timestamp) REPORTING::input FASTQ_R1 filtered reads=${FILTERED_READS_R1_DIFF}"
  echo "$(timestamp) REPORTING::input FASTQ_R2 filtered reads=${FILTERED_READS_R2_DIFF}"

else
  echo "$(timestamp) [ metagen-fastqc ] Cleaning FASTQ files (single-end mode)"
  trim_galore ${FASTQ_R1} -o "$tmp_dir"
  name=${FASTQ_R1%%.fastq*}

  echo "$(timestamp) [ metagen-fastqc ] Mapping files to host genome: ${REF}"
  bwa mem -M -t ${THREADS} ${REF} $tmp_dir/${name}_trimmed.fq.gz | samtools view -@ ${THREADS_SAM} -f 4 -F 256 -uS - -o $tmp_dir/${name}_unmapped.bam
  samtools sort -@ ${THREADS_SAM} -n $tmp_dir/${name}_unmapped.bam -o $tmp_dir/${name}_unmapped_sorted.bam
  bedtools bamtofastq -i $tmp_dir/${name}_unmapped_sorted.bam -fq $final_result_dir/${name}_clean.fastq

	echo "$(timestamp) [ metagen-fastqc ] Compressing output file"
	gzip "$final_result_dir/${name}"_clean.fastq
	touch "$tags_dir/${name}".fastq.clean

  echo "$(timestamp) [ metagen-fastqc ] Cleaning tmp files"
  rm -f ${name}_unmapped.bam ${name}_unmapped_sorted.bam ${name}_trimmed.fq.gz ${FASTQ_R1}_*txt
  rm -rf "$tmp_dir"

  FILTERED_READS_BEFORE=$(zcat ${FASTQ_R1} | echo $((`wc -l`/4)))
  FILTERED_READS_R2_AFTER=$(zcat ${name}_clean.fastq.gz | echo $((`wc -l`/4)))
  FILTERED_READS_DIFF=$((FILTERED_READS_BEFORE-FILTERED_READS_AFTER))

  echo "$(timestamp) REPORTING::input FASTQ filtered reads=${FILTERED_READS_DIFF}"

fi
