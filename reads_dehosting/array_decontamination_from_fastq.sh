#!/bin/bash
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1
#SBATCH --job-name=array_decontamination_fastq

# exit when any command fails, might be a bad idea when there are writing conflicts on output files?
set -euo pipefail

timestamp() {
  date "+%Y-%m-%d %H:%M:%S"
}

DEHOSTING_JOBID=$(sbatch -A "$SLURM_ACCOUNT" \
  --parsable \
  -o "${OUTFILES_DIR}/${PLATFORM}/decontamination_$(basename "${FASTQ_FILES[$SLURM_ARRAY_TASK_ID]}"%.fastq)_%j_%A.out" \
  --array=0-${#FASTQ_FILES[@]} \
  --export=FASTQ="${FASTQ_FILES[@SLURM_ARRAY_TASK_ID]}",SRA_HUMAN_SCRUBBER_SCRIPT_PATH="$SRA_HUMAN_SCRUBBER_SCRIPT_PATH",CLEAN_FASTQ_DIR="$CLEAN_FASTQ_DIR",DATABASE_PATH="$DATABASE_PATH" \
  "${SRA_HUMAN_SCRUBBER_SBATCH_PATH} ")

echo "$(timestamp) [emergen-decontamination] Submitted job ${DEHOSTING_JOBID} (decontamination of fastq files: $FASTQ_FILES)"