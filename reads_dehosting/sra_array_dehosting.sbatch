#!/usr/bin/bash

#SBATCH --mem=8G
#SBATCH --cpus-per-task=4

module purge
module load samtools/1.15.1
module load python/3.9

echo "$SLURM_ARRAY_TASK_ID"

CURR_FILE="${RAW_FILES[@SLURM_ARRAY_TASK_ID]}"

clean_fastq_filepath () {
  BASENAME=$(basename "${1}")
  echo "$2/$BASENAME"
}


for FILE in $RAW_FILES; do
  echo $FILE
  # If BAM

  # If FASTQ
done


if [[ $CURR_FILE == "*.bam" ]]; then
  BAM=$CURR_FILE
  pe_test=$(samtools view -c -f 0x1 "$BAM")
  bam_basename=$(basename "$BAM" | sed -r 's|^(.*?)\.\w+$|\1|')
  bam_basename_no_extension="${BAM%.bam}"
  bam_dir_name=$(dirname "$BAM")
  bam_subdir_name=$(python3 -c "import os; print(str($BAM).split('/')[-1)])")
  if [ "${pe_test}" -ne 0 ]; then
    echo "$(timestamp) [emergen-decontamination] $BAM contains paired-end data"
    samtools collate "$BAM" -o "${bam_basename_no_extension}.collate.bam"
    samtools fastq -@ 4 "${bam_basename_no_extension}.collate.bam" -1 "${bam_basename_no_extension}_R1.fastq.gz" -2 "${bam_basename_no_extension}_R2.fastq.gz" -s "${bam_basename_no_extension}_singletons.fastq.gz"
    rm "${bam_basename_no_extension}.collate.bam"  # Delete collated bam file
    # Output fastq files (raw)
    R1="${bam_basename_no_extension}_R1.fastq.gz"
    R2="${bam_basename_no_extension}_R2.fastq.gz"
    SINGLETONS="${bam_basename_no_extension}_singletons.gz"
    # Set clean fastq files paths
    R1_OUTPUT_FASTQ_PATH=$(clean_fastq_filepath "$R1" "$CLEAN_FASTQ_DIR")
    R2_OUTPUT_FASTQ_PATH=$(clean_fastq_filepath "$R2" "$CLEAN_FASTQ_DIR")
    SINGLETONS_OUTPUT_FASTQ_PATH=$(clean_fastq_filepath "$SINGLETONS" "$CLEAN_FASTQ_DIR")
    # Run dehosting for PE files from BAM
    $SRA_HUMAN_SCRUBBER_SCRIPT_PATH -i "${R1}" -o "$R1_OUTPUT_FASTQ_PATH" -p "$SLURM_CPUS_PER_TASK" -d "$DATABASE_PATH"
    $SRA_HUMAN_SCRUBBER_SCRIPT_PATH -i "${R2}" -o "$R2_OUTPUT_FASTQ_PATH" -p "$SLURM_CPUS_PER_TASK" -d "$DATABASE_PATH"
    $SRA_HUMAN_SCRUBBER_SCRIPT_PATH -i "${SINGLETONS}" -o "$SINGLETONS_OUTPUT_FASTQ_PATH" -p "$SLURM_CPUS_PER_TASK" -d "$DATABASE_PATH"
  else
    echo "$(timestamp) [emergen-decontamination] $BAM contains single-end data"
    samtools collate "$BAM" -o "${bam_basename_no_extension}.collate.bam"
    samtools fastq "${bam_basename}.collate.bam" -1 "${bam_basename_no_extension}.fastq.gz"
    SE="${bam_basename_no_extension}.fastq.gz"
    SE_OUTPUT_FASTQ_PATH=$(clean_fastq_filepath "$SE" "$CLEAN_FASTQ_DIR")
    $SRA_HUMAN_SCRUBBER_SCRIPT_PATH -i "${SE}" -o "$SE_OUTPUT_FASTQ_PATH" -p "$SLURM_CPUS_PER_TASK" -d "$DATABASE_PATH"
  fi
elif [[ $CURR_FILE == "*.*q*" ]]; then
  echo "FASTQ"
  FASTQ=$CURR_FILE
  OUTPUT_FASTQ_PATH=$(clean_fastq_filepath "$FASTQ" "$CLEAN_FASTQ_DIR")
  $SRA_HUMAN_SCRUBBER_SCRIPT_PATH -i "${FASTQ}" -o "$OUTPUT_FASTQ_PATH" -p "$SLURM_CPUS_PER_TASK" -d "$DATABASE_PATH"
fi