#!/bin/bash
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1
#SBATCH --job-name=batch_decontamination_bam

# exit when any command fails
set -e
set -o pipefail

timestamp() {
  date "+%Y-%m-%d %H:%M:%S"
}

scriptdir=$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )

echo $bam_files
decontamination_array=$(sbatch -A "emergen_ifb" -o "${outfile_dir}/${platform}/batch_decontamination_%j.out" --parsable --array=0-${#bam_files[@]} --export=bam="${bam_files}",subdir="${subdir}",scriptdir="${scriptdir}",platform="${platform}",outfile_dir="${outfile_dir}",hg38_ref="${hg38_ref}" "${scriptdir}/array_decontamination_from_bam.sh")
echo "$(timestamp) - Submitted job array ${decontamination_array} (human sequences decontamination from bam files)"
