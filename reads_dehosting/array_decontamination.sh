#!/bin/bash
#SBATCH --mem=8G
#SBATCH --cpus-per-task=4
#SBATCH --job-name=array_decontamination_bam

# exit when any command fails, might be a bad idea when there are writing conflicts on output files?
set -euo pipefail

timestamp() {
  date "+%Y-%m-%d %H:%M:%S"
}

module purge
module load samtools/1.15.1
module load python/3.9

if [ -z ${ALL_FASTQ_FILES+x} ]; then
  if (( ${#ALL_BAM_FILES[@]} )); then
    for BAM in $ALL_BAM_FILES; do
      if [[ $BAM != *"clean"* ]]; then
        pe_test=$(samtools view -c -f 0x1 "$BAM")
        bam_basename=$(basename "$BAM" | sed -r 's|^(.*?)\.\w+$|\1|')
        bam_basename_no_extension="${BAM%.bam}"
        bam_dir_name=$(dirname "$BAM")
        bam_subdir_name=$(python3 -c "import os; print(str($BAM).split('/')[-1)])")
        if [ "${pe_test}" -ne 0 ]; then
          echo "$(timestamp) [emergen-decontamination] $BAM contains paired-end data"
          samtools collate "$BAM" -o "${bam_basename_no_extension}.collate.bam"
          samtools fastq -@ 4 "${bam_basename_no_extension}.collate.bam" -1 "${bam_basename_no_extension}_R1.fastq.gz" -2 "${bam_basename_no_extension}_R2.fastq.gz" -s "${bam_basename_no_extension}_singletons.fastq.gz"
          rm "${bam_basename_no_extension}.collate.bam"  # Delete collated bam file
          R1="${bam_basename_no_extension}_R1.fastq.gz"
          R2="${bam_basename_no_extension}_R2.fastq.gz"
          SINGLETONS="${bam_basename_no_extension}_singletons.gz"
#          NB_READS_R1=$(echo $(zcat "$R1" | wc -l)/4 | bc)
#          NB_READS_R2=$(echo $(zcat "$R1" | wc -l)/4 | bc)
#          NB_READS_SINGLETONS=$(echo "$(zcat "$SINGLETONS" | wc -l)"/4 | bc)
          if [[ -n $R1 ]]; then
            ALL_FASTQ_FILES+=("$R1")
          fi
          if [[ -n $R2 ]]; then
            ALL_FASTQ_FILES+=("$R2")
          fi
          if [[ -n $SINGLETONS ]]; then
            ALL_FASTQ_FILES+=("$SINGLETONS")
          fi
        else
          echo "$(timestamp) [emergen-decontamination] $BAM contains single-end data"
          samtools collate "$BAM" -o "${bam_basename_no_extension}.collate.bam"
          samtools fastq "${bam_basename}.collate.bam" -1 "${bam_basename_no_extension}.fastq.gz"
          SE="${bam_basename_no_extension}.fastq.gz"
#          NB_READS_SE=$(echo "$(zcat "$SE" | wc -l)"/4 | bc)
          # Add to fastq array
          if [[ -n $SE ]]; then
            ALL_FASTQ_FILES+=("$SE")
          fi
        fi
      fi
    done
  fi
  if (( ${#ALL_FASTQ_FILES[@]} )); then
    DEHOSTING_JOBID=$(sbatch -A "$SLURM_ACCOUNT" \
    --parsable \
    -o "${OUTFILES_DIR}/${PLATFORM}/decontamination_%j_%A.out" \
    --array=0-${#ALL_FASTQ_FILES[@]} \
    --export=FASTQ="${ALL_FASTQ_FILES[$SLURM_ARRAY_TASK_ID]}",SRA_HUMAN_SCRUBBER_SCRIPT_PATH="$SRA_HUMAN_SCRUBBER_SCRIPT_PATH",CLEAN_FASTQ_DIR="$CLEAN_FASTQ_DIR",DATABASE_PATH="$DATABASE_PATH" \
    "${SRA_HUMAN_SCRUBBER_SBATCH_PATH} ")
    echo "$(timestamp) [emergen-decontamination] Submitted dehosting job ${DEHOSTING_JOBID}"
  fi
else
  echo "No fastq files supplied for dehosting, exiting"
  exit 1
fi