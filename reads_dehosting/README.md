# decontamination_pipeline

Scripts to detect and treat new raw sequencing data files on the various EMERGEN project spaces

Contain [an implementation of the metagen_fastq](!https://github.com/Finn-Lab/Metagen-FastQC) pipeline: [metagen_fastq.sh](/reads_dehosting/metagen_fastqc.sh) 
that is used to remove human reads from fastq data. 
BWA is used to perform the alignments and trim-galore for reads trimming.

The pipeline is submitted to a HPC with the slurm scheduler, as it makes use of slurm job arrays functionality.  

- [batch_decontamination_from_bam.sh](/reads_dehosting/batch_decontamination_from_bam.sh): wrapper for the dehosting 
  pipeline taking as input multiple BAM files. Submits an array of jobs using
  [array_decontamination_from_bam.sh](/reads_dehosting/array_decontamination_from_bam.sh)
- [batch_decontamination_from_fastq.sh](/reads_dehosting/batch_decontamination_from_fastq.sh): wrapper for the dehosting
  pipeline taking as input fastq files (both paired-end and single-end). Submits
  an array of jobs using 
  [array_decontamination_from_fastq.sh](/reads_dehosting/array_decontamination_from_fastq.sh)
