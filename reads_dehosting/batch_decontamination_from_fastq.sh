#!/bin/bash
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1
#SBATCH --job-name=batch_decontamination_fastq

# exit when any command fails
set -e
set -o pipefail

scriptdir=$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )

echo $r1_files
echo $r2_files
echo $se_files

decontamination_array=$(sbatch -A "emergen_ifb" "${outfile_dir}/${platform}/batch_decontamination_%j.out" --parsable --array=0-${#r1_files[@]} --export=r1_files="${r1_files}",r2_files="${r2_files}",se_files="${se_files}",platform="${platform}",topdir_name="${topdir_name}",subdir="${subdir}",scriptdir="${scriptdir}",outfile_dir="${outfile_dir}",hg38_ref="${hg38_ref}" "${scriptdir}/array_decontamination_from_fastq.sh")
echo "$(timestamp) - Submitted job array ${decontamination_array} (human sequences decontamination from bam files)"
