#!/bin/bash
#SBATCH --mem=8G
#SBATCH --cpus-per-task=4

# exit when any command fails
set -euxo pipefail

timestamp() {
  date "+%Y-%m-%d %H:%M:%S"
}

module purge
module load samtools/1.15.1
module load python/3.9

if [[ $BAM != *"clean"* ]]; then

  pe_test=$(samtools view -c -f 0x1 "$BAM")
  bam_basename=$(basename "$BAM" | sed -r 's|^(.*?)\.\w+$|\1|')
  bam_basename_no_extension="${BAM%.bam}"
  bam_dir_name=$(dirname "$BAM")
  bam_subdir_name=$(python3 -c "import os; print(str($BAM).split('/')[-1)])")

  if [ "${pe_test}" -ne 0 ]; then

    echo "$(timestamp) [emergen-decontamination] $BAM is paired-end"
    samtools collate "$BAM" -o "${bam_basename_no_extension}.collate.bam"
    samtools fastq -@ 4 "${bam_basename_no_extension}.collate.bam" -1 "${bam_basename_no_extension}_R1.fastq.gz" -2 "${bam_basename_no_extension}_R2.fastq.gz" -s "${bam_basename_no_extension}_singletons.fastq.gz"
    rm "${bam_basename_no_extension}.collate.bam"  # Delete collated bam file

    R1="${bam_basename_no_extension}_R1.fastq.gz"
    R2="${bam_basename_no_extension}_R2.fastq.gz"
    SINGLETONS="${bam_basename_no_extension}_singletons.gz"

    DEHOSTING_JOBID_R1=$(sbatch -A "$SLURM_ACCOUNT" \
      --parsable \
      -o "${OUTFILES_DIR}/${PLATFORM}/decontamination_$(basename "$R1"_%j.out)" \
      --export=FASTQ="$R1",SRA_HUMAN_SCRUBBER_SCRIPT_PATH="$SRA_HUMAN_SCRUBBER_SCRIPT_PATH",CLEAN_FASTQ_DIR="$CLEAN_FASTQ_DIR",DATABASE_PATH="$DATABASE_PATH" \
      "${SRA_HUMAN_SCRUBBER_SBATCH_PATH} ")
    echo "$(timestamp) [emergen-decontamination] Submitted job ${DEHOSTING_JOBID_R1} (decontamination of fastq file $R1 extracted from bam file $BAM)"

    DEHOSTING_JOBID_R2=$(sbatch -A "$SLURM_ACCOUNT" \
      --parsable \
      -o "${OUTFILES_DIR}/${PLATFORM}/decontamination_$(basename "$R2"_%j.out)" \
      --export=FASTQ="$R1",SRA_HUMAN_SCRUBBER_SCRIPT_PATH="$SRA_HUMAN_SCRUBBER_SCRIPT_PATH",CLEAN_FASTQ_DIR="$CLEAN_FASTQ_DIR",DATABASE_PATH="$DATABASE_PATH" \
      "${SRA_HUMAN_SCRUBBER_SBATCH_PATH} ")
    echo "$(timestamp) [emergen-decontamination] Submitted job ${DEHOSTING_JOBID_R2} (decontamination of fastq file $R2 extracted from bam file $BAM)"

    DEHOSTING_JOBID_SINGLETONS=$(sbatch -A "$SLURM_ACCOUNT" \
      --parsable \
      -o "${OUTFILES_DIR}/${PLATFORM}/decontamination_$(basename "$SINGLETONS"_%j.out)" \
      --export=FASTQ="$R1",SRA_HUMAN_SCRUBBER_SCRIPT_PATH="$SRA_HUMAN_SCRUBBER_SCRIPT_PATH",CLEAN_FASTQ_DIR="$CLEAN_FASTQ_DIR",DATABASE_PATH="$DATABASE_PATH" \
      "${SRA_HUMAN_SCRUBBER_SBATCH_PATH} ")
    echo "$(timestamp) [emergen-decontamination] Submitted job ${DEHOSTING_JOBID_SINGLETONS} (decontamination of fastq file $SINGLETONS extracted from bam file $BAM)"

  else

    echo "$(timestamp) [emergen-decontamination] $BAM is single-end"
    samtools collate "$BAM" -o "${bam_basename_no_extension}.collate.bam"
    samtools fastq "${bam_basename}.collate.bam" -1 "${bam_basename}.fastq.gz"

    SE_FASTQ="${bam_basename_no_extension}.fastq.gz"

    DEHOSTING_JOBID_SE_FASTQ=$(sbatch -A "$SLURM_ACCOUNT" \
      --parsable \
      -o "${OUTFILES_DIR}/${PLATFORM}/decontamination_$(basename "$SE_FASTQ"_%j.out)" \
      --export=FASTQ="$SE_FASTQ",SRA_HUMAN_SCRUBBER_SCRIPT_PATH="$SRA_HUMAN_SCRUBBER_SCRIPT_PATH",CLEAN_FASTQ_DIR="$CLEAN_FASTQ_DIR",DATABASE_PATH="$DATABASE_PATH" \
      "${SRA_HUMAN_SCRUBBER_SBATCH_PATH} ")
    echo "$(timestamp) [emergen-decontamination] Submitted job ${DEHOSTING_JOBID_SE_FASTQ} (decontamination of fastq file $SE_FASTQ extracted from bam file $BAM)"

  fi
fi
