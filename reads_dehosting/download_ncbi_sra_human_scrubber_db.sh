#!/bin/bash
#SBATCH --mem=4G
#SBATCH --cpus-per-task=1

set -euxo pipefail

VERSION=$(curl "https://ftp.ncbi.nlm.nih.gov/sra/dbs/human_filter/current/version.txt")
curl -vf "https://ftp.ncbi.nlm.nih.gov/sra/dbs/human_filter/${VERSION}.human_filter.db" -o "/shared/emergen-airflow/airflow/data/${VERSION}.human_filter.db"
unlink /shared/emergen-airflow/airflow/data/human_filter.db && ln -s "/shared/emergen-airflow/airflow/data/${VERSION}".human_filter.db /shared/emergen-airflow/airflow/data/human_filter.db

exit 0
