#!/bin/bash
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1

module purge
module load nextflow
module load bwa
module load pysam
module load csvkit

nextflow run "$NF_PIPELINE_PATH" -profile conda --illumina --directory "$INPUT_DIR" --human_ref "$HUMAN_RED_GENOME_PATH"
