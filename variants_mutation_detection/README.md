# variants_mutation_detection

Scripts to supply/update the Nextclade alignments of the datalake.

Aligned sequences are extracted automatically from Nextclade results.

- [Generate the datalake Nextclade alignements](/variants_mutation_detection/update_datalake.py)
- [Update the datalake Nextclade alignments](/variants_mutation_detection/update_datalake.py)

