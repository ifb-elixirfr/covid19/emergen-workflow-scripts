#!/usr/bin/python3

import shlex
import shutil
import subprocess
import sys
import os
import argparse
import logging
import gzip
import csv

from Bio import SeqIO
from collections import defaultdict
from glob import glob

# Create a logger and set minimal level to debug
logger = logging.getLogger("emergen-datalake")
logger.setLevel(logging.DEBUG)

# Create a console handler and set minimal level to debug
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# Format for the logging message
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# Set the format for the console handler
console_handler.setFormatter(formatter)
# Affect console handler to the logger
logger.addHandler(console_handler)


def add_to_gene_alignment_file(
    gene, gene_alignments_file, gene_alignments_datalake_path
):
    """
    Add to the output gene alignment file (gzip)

    :param gene:
    :param gene_alignments_file:
    :param gene_alignments_datalake_path:
    :return:
    """

    with open(gene_alignments_file, "r") as fr_handle:
        # logger.info(f"Appending results from Nextclade alignments file {gene_alignments_file} to gene {gene} alignments datalake")
        with gzip.open(gene_alignments_datalake_path, "ab") as fw_handle:
            for line in fr_handle:
                fw_handle.write(line.encode())
            return True


def add_to_tab_file(alignments_file, datalake_tab_path):
    """

    :param alignments_file:
    :param datalake_tab_path:
    :return:
    """

    content_list = []
    tmp_line = ""
    try:
        with open(alignments_file, "r") as fr_handle:
            for line in fr_handle:
                if line.startswith(">"):
                    content_list.append(tmp_line)
                    tmp_line = line.replace(">", "").strip() + "\t"
                else:
                    tmp_line += line.strip()
        with gzip.open(datalake_tab_path, "ab") as fw_handle:
            fw_handle.write("\n".join(content_list).encode())
    except Exception:
        logger.warning(f"Failed writing to tsv.gz file {datalake_tab_path}")


def add_to_genome_alignment_file(
    genome_alignments_file, genome_alignments_datalake_path
):
    """
    Add to the genome output alignment file (gzip)

    :param genome_alignments_file:
    :param genome_alignments_datalake_path:
    :return:
    """

    with open(genome_alignments_file, "r") as fr_handle:
        # logger.info(f"Appending results from Nextclade alignments file {genome_alignments_file} to genome alignments datalake")
        with gzip.open(genome_alignments_datalake_path, "ab") as fw_handle:
            for line in fr_handle:
                fw_handle.write(line.encode())
            return True


def create_tmp_alignment_file(alignment_file):
    shutil.move(alignment_file, f"{alignment_file}.swap")


class DatalakeUpdate:
    def __init__(self, genes, mode, datestamp):
        self.emergen_datalake_directory_path = (
            f"/shared/projects/emergen_ifb/emergen_datalake"
        )
        self.genes = genes
        self.nextclade_pangolin_pipeline_directory = (
            "/shared/projects/emergen_ifb/nextclade_pangolin_working_dir"
        )
        self.current_nextclade_results_directory = f"{self.nextclade_pangolin_pipeline_directory}/nextclade_pangolin_{mode}_{datestamp}/results/nextclade"

    def update_genome_alignments(self):
        current_partial_results_directory_nextclade_results_runs_directories = glob(
            f"{self.current_nextclade_results_directory}/nextclade_run*"
        )
        for (
            nextclade_run_directory
        ) in current_partial_results_directory_nextclade_results_runs_directories:
            for gene in self.genes:
                gene_alignments = glob(f"{nextclade_run_directory}/*gene.{gene}.fasta")
                if gene_alignments:
                    # Append to fasta.gz file
                    emergen_datalake_gene_alignments_fasta_file = f"{self.emergen_datalake_directory_path}/nextclade_alignments/proteins/gene_{gene}_alignments.fasta.gz"
                    add_to_gene_alignment_file(
                        gene=gene,
                        gene_alignments_file=gene_alignments[0],
                        gene_alignments_datalake_path=emergen_datalake_gene_alignments_fasta_file,
                    )
                    # remove_duplicates_fasta(alignment_file=emergen_datalake_gene_alignments_fasta_file)
                    logger.info(
                        f"Updated datalake gene {gene} alignments fasta file {emergen_datalake_gene_alignments_fasta_file} with alignments from {gene_alignments[0]}"
                    )

                    # Append to tsv.gz file
                    emergen_datalake_gene_alignments_tab_file = f"{self.emergen_datalake_directory_path}/nextclade_alignments/proteins/gene_{gene}_alignments.tsv.gz"
                    add_to_tab_file(
                        alignments_file=gene_alignments[0],
                        datalake_tab_path=emergen_datalake_gene_alignments_tab_file,
                    )
                    logger.info(
                        f"Updated datalake gene {gene} alignments tsv file {emergen_datalake_gene_alignments_fasta_file} with alignments from {gene_alignments[0]}"
                    )
                else:
                    logger.info(
                        f"Could not find an alignment file for gene {gene} in nextclade results directory {nextclade_run_directory} (SKIPPING)"
                    )

            # Append to fasta.gz file
            emergen_datalake_genome_alignments_fasta_file = f"{self.emergen_datalake_directory_path}/nextclade_alignments/genomes/genome_alignments.fasta.gz"
            genome_alignment_file = glob(f"{nextclade_run_directory}/*aligned.fasta")
            if genome_alignment_file:
                add_to_genome_alignment_file(
                    genome_alignments_file=genome_alignment_file[0],
                    genome_alignments_datalake_path=emergen_datalake_genome_alignments_fasta_file,
                )
                # remove_duplicates_fasta(alignment_file=emergen_datalake_genome_alignments_fasta_file)
                logger.info(
                    f"Updated datalake genome alignments fasta file {emergen_datalake_genome_alignments_fasta_file} with alignments from {genome_alignment_file[0]}"
                )

                # Append to tsv gz file
                emergen_datalake_genome_alignments_tab_file = f"{self.emergen_datalake_directory_path}/nextclade_alignments/genomes/genome_alignments.tsv.gz"
                add_to_tab_file(
                    alignments_file=genome_alignment_file[0],
                    datalake_tab_path=emergen_datalake_genome_alignments_tab_file,
                )
                logger.info(
                    f"Updated datalake genome alignments tsv file {emergen_datalake_genome_alignments_tab_file} with alignments from {genome_alignment_file[0]}"
                )
            else:
                logger.info(
                    f"Could not find genome alignments file for nextclade results directory {nextclade_run_directory} (SKIPPING)"
                )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Update emergen datalake with latest alignment results"
    )
    parser.add_argument("--mode", type=str, help="Either partial or all")
    parser.add_argument("--date", type=str, help="Datestamp in format YYYY_MM_DD")
    args = parser.parse_args()

    ds = DatalakeUpdate(
        genes=[
            "E",
            "S",
            "M",
            "N",
            "ORF1a",
            "ORF1b",
            "ORF3a",
            "ORF6",
            "ORF7a",
            "ORF7b",
            "ORF8",
            "ORF9b",
        ],
        mode=args.mode,
        datestamp=args.date,
    )

    ds.update_genome_alignments()  # Update datalake
