#!/usr/bin/python3

import os
import logging
import gzip
import shutil
import subprocess
import shlex

from glob import glob
from Bio import SeqIO

# Create a logger and set minimal level to debug
logger = logging.getLogger("emergen-datalake")
logger.setLevel(logging.DEBUG)

# Create a console handler and set minimal level to debug
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# Format for the logging message
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# Set the format for the console handler
console_handler.setFormatter(formatter)
# Affect console handler to the logger
logger.addHandler(console_handler)


def add_to_gene_alignment_file(
    gene, gene_alignments_file, gene_alignments_datalake_path, mode
):
    """
    Add (write or append) to the output gene alignment file

    :param gene:
    :param gene_alignments_file:
    :param gene_alignments_datalake_path:
    :param mode:
    :return:
    """

    with open(gene_alignments_file, "r") as fr_handle:
        logger.info(
            f"Appending results from Nextclade alignments file {gene_alignments_file} to gene {gene} alignments datalake"
        )
        with open(gene_alignments_datalake_path, f"{mode}") as fw_handle:
            for line in fr_handle:
                fw_handle.write(line)
    return True


def add_to_genome_alignment_file(
    genome_alignments_file, genome_alignments_datalake_path, mode
):
    """
    Add (write or append) to the genome output alignment file

    :param genome_alignments_file:
    :param genome_alignments_datalake_path:
    :param mode:
    :return:
    """

    with open(genome_alignments_file, "r") as fr_handle:
        logger.info(
            f"Appending results from Nextclade alignments file {genome_alignments_file} to genome alignments datalake"
        )
        with open(genome_alignments_datalake_path, f"{mode}") as fw_handle:
            for line in fr_handle:
                fw_handle.write(line)
    return True


def compress_list_of_files(files_list, block_size):
    """
    gz compression for input list of files
    Original files are deleted on successful compression

    :param files_list:
    :param block_size:
    :return:
    """
    for file in files_list:
        logger.info(f"Compressing file {file}")
        # delete_original_file = False
        compression_cl = shlex.split(f"gzip {file}")
        subprocess.Popen(compression_cl)

        # try:
        #     with open(file, 'rb') as infile:
        #         with gzip.open(f"{file}.gz", 'wb') as outfile:
        #             while True:
        #                 content = infile.read(block_size)
        #                 if not content:
        #                     break
        #                 outfile.write(content)
        #     delete_original_file = True
        # except Exception:
        #     logger.error(f"Failed compressing file {file}")

        # if delete_original_file:
        #     logger.info(f"Successfully compressed {file}, deleting original uncompressed file")
        #     os.remove(file)


class DatalakeSupplier:
    def __init__(self, genes):
        self.emergen_datalake_directory_path = (
            f"/shared/projects/emergen_ifb/emergen_datalake"
        )
        self.genes = genes
        self.nextclade_pangolin_pipeline_directory = (
            "/shared/projects/emergen_ifb/nextclade_pangolin_working_dir"
        )
        self.results_directories = self.set_results_directories()
        self.all_results_directory = self.results_directories["all_results_dir"]
        self.partial_results_directories = self.results_directories[
            "partial_results_dirs"
        ]

    def create_datalake_dirs(self):
        try:
            os.makedirs(
                f"{self.emergen_datalake_directory_path}/nextclade_alignments/proteins"
            )
        except FileExistsError:
            logger.debug(
                f"Datalake directory for Nextclade proteins alignments already exists"
            )

        try:
            os.makedirs(
                f"{self.emergen_datalake_directory_path}/nextclade_alignments/genomes"
            )
        except FileExistsError:
            logger.debug(
                f"Datalake directory for Nextclade genomes alignments already exists"
            )

    def set_results_directories(self):
        """
        Set Nextclade results directories (all sequences and partial sequences)

        :return:
        """
        nextclade_pangolin_all_results_directories_list = glob(
            f"{self.nextclade_pangolin_pipeline_directory}/" f"nextclade_pangolin_all*"
        )
        latest_all_results_directory_date = 0

        for directory in nextclade_pangolin_all_results_directories_list:
            directory_date = int(
                directory.split("nextclade_pangolin_all_")[1].replace("_", "")
            )
            if directory_date > latest_all_results_directory_date:
                latest_all_results_directory_date = directory_date

        latest_all_results_directory_date_formatted = str(
            latest_all_results_directory_date
        )
        latest_all_results_directory_date_formatted = (
            f"{latest_all_results_directory_date_formatted[0:4]}_"
            f"{latest_all_results_directory_date_formatted[4:6]}_"
            f"{latest_all_results_directory_date_formatted[6:8]}_"
            f"{latest_all_results_directory_date_formatted[8:10]}_"
            f"{latest_all_results_directory_date_formatted[10:12]}"
        )

        latest_all_results_directory = (
            f"{self.nextclade_pangolin_pipeline_directory}/"
            f"nextclade_pangolin_all_{latest_all_results_directory_date_formatted}"
        )
        logger.info(
            f"Latest complete Nextclade results directory: {latest_all_results_directory}"
        )

        latest_all_results_directory_nextclade_results_directory = (
            f"{latest_all_results_directory}/results/nextclade"
        )

        partial_results_directories_list = []
        partial_results_directory_list = glob(
            f"{self.nextclade_pangolin_pipeline_directory}/nextclade_pangolin_partial*"
        )
        for directory in partial_results_directory_list:
            directory_date = int(
                directory.split("nextclade_pangolin_partial_")[1].replace("_", "")
            )
            if directory_date > latest_all_results_directory_date:
                current_partial_results_directory_date_formatted = str(directory_date)
                current_partial_results_directory_date_formatted = (
                    f"{current_partial_results_directory_date_formatted[0:4]}_"
                    f"{current_partial_results_directory_date_formatted[4:6]}_"
                    f"{current_partial_results_directory_date_formatted[6:8]}_"
                    f"{current_partial_results_directory_date_formatted[8:10]}_"
                    f"{current_partial_results_directory_date_formatted[10:12]}"
                )
                current_partial_results_directory = f"{self.nextclade_pangolin_pipeline_directory}/nextclade_pangolin_partial_{current_partial_results_directory_date_formatted}"
                current_partial_results_directory_nextclade_results_directory = (
                    f"{current_partial_results_directory}/results/nextclade"
                )
                partial_results_directories_list.append(
                    current_partial_results_directory_nextclade_results_directory
                )

        return {
            "all_results_dir": latest_all_results_directory_nextclade_results_directory,
            "partial_results_dirs": partial_results_directories_list,
        }

    def generate_datalake(self):
        """
        Generate the concatenated alignments files in the datalake

        :return:
        """

        latest_all_results_directory_nextclade_results_subdirectories_list = glob(
            f"{self.all_results_directory}/nextclade_run*"
        )

        # Create a new alignment files from first nextclade run
        for (
            nextclade_run_directory
        ) in latest_all_results_directory_nextclade_results_subdirectories_list:
            if nextclade_run_directory.endswith("_1"):
                for gene in self.genes:
                    gene_alignments = glob(
                        f"{nextclade_run_directory}/*gene.{gene}.fasta"
                    )
                    try:
                        add_to_gene_alignment_file(
                            gene=gene,
                            gene_alignments_file=gene_alignments[0],
                            mode="w",
                            gene_alignments_datalake_path=f"{self.emergen_datalake_directory_path}/nextclade_alignments/proteins/gene_{gene}_alignments.fasta",
                        )
                    except IndexError:
                        logger.error(
                            f"Missing gene {gene} alignments file for results directory {nextclade_run_directory}"
                        )

                genome_alignment_file = glob(
                    f"{nextclade_run_directory}/*aligned.fasta"
                )
                try:
                    add_to_genome_alignment_file(
                        genome_alignments_file=genome_alignment_file[0],
                        mode="w",
                        genome_alignments_datalake_path=f"{self.emergen_datalake_directory_path}/nextclade_alignments/genomes/genome_alignments.fasta",
                    )
                except IndexError:
                    logger.error(
                        f"Missing genome alignments file for results directory {nextclade_run_directory}"
                    )

        # Append the rest of alignments from nextclade complete results runs
        for (
            nextclade_run_directory
        ) in latest_all_results_directory_nextclade_results_subdirectories_list:
            if not nextclade_run_directory.endswith("_1"):
                for gene in self.genes:
                    gene_alignments = glob(
                        f"{nextclade_run_directory}/*gene.{gene}.fasta"
                    )
                    try:
                        add_to_gene_alignment_file(
                            gene=gene,
                            gene_alignments_file=gene_alignments[0],
                            mode="a",
                            gene_alignments_datalake_path=f"{self.emergen_datalake_directory_path}/nextclade_alignments/proteins/gene_{gene}_alignments.fasta",
                        )
                    except IndexError:
                        logger.error(
                            f"Missing gene {gene} alignments file for results directory {nextclade_run_directory}"
                        )

                genome_alignment_file = glob(
                    f"{nextclade_run_directory}/*aligned.fasta"
                )
                try:
                    add_to_genome_alignment_file(
                        genome_alignments_file=genome_alignment_file[0],
                        mode="a",
                        genome_alignments_datalake_path=f"{self.emergen_datalake_directory_path}/nextclade_alignments/genomes/genome_alignments.fasta",
                    )
                except IndexError:
                    logger.error(
                        f"Missing genome alignments file for results directory {nextclade_run_directory}"
                    )

        # Append partial results alignments
        for (
            current_partial_results_directory_nextclade_results_directories
        ) in self.partial_results_directories:
            current_partial_results_directory_nextclade_results_runs_directories = glob(
                f"{current_partial_results_directory_nextclade_results_directories}/nextclade_run*"
            )
            for (
                nextclade_run_directory
            ) in current_partial_results_directory_nextclade_results_runs_directories:
                for gene in self.genes:
                    gene_alignments = glob(
                        f"{nextclade_run_directory}/*gene.{gene}.fasta"
                    )
                    try:
                        add_to_gene_alignment_file(
                            gene=gene,
                            gene_alignments_file=gene_alignments[0],
                            mode="a",
                            gene_alignments_datalake_path=f"{self.emergen_datalake_directory_path}/nextclade_alignments/proteins/gene_{gene}_alignments.fasta",
                        )
                    except IndexError:
                        logger.error(
                            f"Missing gene {gene} alignments file for results directory {nextclade_run_directory}"
                        )

                genome_alignment_file = glob(
                    f"{nextclade_run_directory}/*aligned.fasta"
                )
                try:
                    add_to_genome_alignment_file(
                        genome_alignments_file=genome_alignment_file[0],
                        mode="a",
                        genome_alignments_datalake_path=f"{self.emergen_datalake_directory_path}/nextclade_alignments/genomes/genome_alignments.fasta",
                    )
                except IndexError:
                    logger.error(
                        f"Missing genome alignments file for results directory {nextclade_run_directory}"
                    )

    def convert_to_tsv(self):
        """
        Convert a fasta file to tsv (<id>\t<sequence>)

        :return:
        """

        datalake_alignments_files = glob(
            f"{self.emergen_datalake_directory_path}/*/*/*.fasta"
        )

        for alignment_file in datalake_alignments_files:
            tab_file = alignment_file.split(".fasta")[0]
            logger.info(f"Converting file {alignment_file} to tsv")
            content_list = []
            tmp_line = ""
            try:
                with open(alignment_file, "r") as fr_handle:
                    for line in fr_handle:
                        if line.startswith(">"):
                            content_list.append(tmp_line)
                            tmp_line = line.replace(">", "").strip() + "\t"
                        else:
                            tmp_line += line.strip()
                tab_file = f"{tab_file}.tsv"
                with open(tab_file, "w") as fw_handle:
                    fw_handle.write("\n".join(content_list))
            except Exception:
                logger.error(f"Failed converting file {alignment_file} to tsv")

    def remove_duplicates_fasta(self):
        datalake_alignments_files = glob(
            f"{self.emergen_datalake_directory_path}/*/*/*.fasta"
        )

        for alignment_file in datalake_alignments_files:
            sequences_seen = set()
            records = []

            # Create swap file
            shutil.move(alignment_file, f"{alignment_file}.swap")

            swap_fhandle = open(f"{alignment_file}.swap", "r")
            output_fhandle = open(alignment_file, "w")

            for record in SeqIO.parse(swap_fhandle, "fasta"):
                if record.id not in sequences_seen:
                    sequences_seen.add(record.id)
                    records.append(record)

            SeqIO.write(records, output_fhandle, format="fasta")

            swap_fhandle.close()
            output_fhandle.close()

            os.remove(f"{alignment_file}.swap")

    def remove_duplicates_tsv(self):
        datalake_tab_files = glob(f"{self.emergen_datalake_directory_path}/*/*/*.tsv")

        for tsv_file in datalake_tab_files:
            shutil.move(tsv_file, f"{tsv_file}.swap")
            swap_fhandle = open(f"{tsv_file}.swap", "r")
            output_fhandle = open(f"{tsv_file}", "w")

            sequences_seen = []

            for row in swap_fhandle:
                if row in sequences_seen:
                    continue
                else:
                    output_fhandle.write(row)
                    sequences_seen.append(row)

            swap_fhandle.close()
            output_fhandle.close()

            os.remove(f"{tsv_file}.swap")

    def compress_and_delete_datalake_files(self, block_size):
        """
        Compress datalake alignments files and delete original uncompressed files

        :return:
        """

        logger.info(f"Compressing fasta files")
        datalake_alignments_files = glob(
            f"{self.emergen_datalake_directory_path}*.fasta"
        )
        compress_list_of_files(datalake_alignments_files, block_size=block_size)

        logger.info(f"Compressing tsv files")
        datalake_tab_files = glob(f"{self.emergen_datalake_directory_path}*.tsv")
        compress_list_of_files(datalake_tab_files, block_size=block_size)


if __name__ == "__main__":
    ds = DatalakeSupplier(
        genes=[
            "E",
            "S",
            "M",
            "N",
            "ORF1a",
            "ORF1b",
            "ORF3a",
            "ORF6",
            "ORF7a",
            "ORF7b",
            "ORF8",
            "ORF9b",
        ]
    )
    ds.create_datalake_dirs()
    ds.generate_datalake()
    ds.convert_to_tsv()
    ds.remove_duplicates_fasta()
    ds.remove_duplicates_tsv()
    ds.compress_and_delete_datalake_files(block_size=4096)

    console_handler.close()
