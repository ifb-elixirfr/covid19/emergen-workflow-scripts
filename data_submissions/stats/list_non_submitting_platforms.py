#!/usr/bin/env python3

import os
import sys
import pandas as pd


def main(infile, outfile):
    df = pd.read_csv(infile, sep="\t", header=0, index_col=False)
    submitting_platforms = []
    non_submitting_platforms = []

    for row in df.itertuples():
        if row.number_submissions == 0:
            non_submitting_platforms.append(row.platform)
        else:
            submitting_platforms.append(row.platform)

    with open(outfile, "w") as handle:
        handle.write("platform\tsubmitting\tcontact\n")
        for x in submitting_platforms:
            handle.write(f"{x}\tyes\tyes\n")
        for x in non_submitting_platforms:
            handle.write(f"{x}\tno\n")


if __name__ == "__main__":
    main(infile=os.path.abspath(sys.argv[1]), outfile=os.path.abspath(sys.argv[2]))
