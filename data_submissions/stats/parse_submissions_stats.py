#!/usr/bin/env python3

import os
import sys
import pandas as pd


def sort_submissions_files_list_by_date(stats_files_list: set or list) -> list:
    date_sorted_list = []
    dates_list = []
    path_to_stats_files = None

    for x in stats_files_list:
        x_date = str(x.split("/")[-1].split(".")[0].split("_")[-1])
        if not path_to_stats_files:
            path_to_stats_files = str(x.split("/raw_data")[0])
        dates_list.append(x_date)

    dates_list.sort()
    for x in dates_list:
        date_sorted_list.append(f"{path_to_stats_files}/raw_data_submissions_{x}.tsv")

    return date_sorted_list


if __name__ == "__main__":
    script_path = sys.argv[0]
    input_dir = os.path.abspath(sys.argv[1])
    output_dir = os.path.abspath(sys.argv[2])

    # All stats files
    all_submissions_stats_files_set = [
        *set(
            [
                os.path.join(input_dir, x)
                for x in next(os.walk(input_dir))[2]
                if ".tsv" in x
            ]
        )
    ]

    earliest_stats_file_date = 99999999
    earliest_stats_file = None
    for x in all_submissions_stats_files_set:
        current_stats_file_date = int(x.split("/")[-1].split(".")[0].split("_")[-1])
        if current_stats_file_date < earliest_stats_file_date:
            earliest_stats_file_date = current_stats_file_date
            earliest_stats_file = x

    # Extract cols
    earliest_stats_df = pd.read_csv(
        all_submissions_stats_files_set[0], sep="\t", header=0, index_col=False
    )
    platforms = earliest_stats_df["platform"]

    # Create final tsv file with values for every week
    weekly_df_dict = {}
    weekly_df_colnames = []
    for x in platforms:
        weekly_df_colnames.append(x)

    weekly_df = pd.DataFrame()
    weekly_df.insert(
        0, "platform", weekly_df_colnames, True
    )  # Create platform index column/row
    weekly_df.index = weekly_df_colnames  # For index location

    del earliest_stats_df

    insert_loc = 1

    sorted_submissions_stats_files_list = sort_submissions_files_list_by_date(
        all_submissions_stats_files_set
    )

    for x in sorted_submissions_stats_files_list:
        x_df = pd.read_csv(x, sep="\t", header=0, index_col=False)
        if (
            "number_recent_samples" in x_df.columns
            and "number_recent_submissions" in x_df.columns
            and "number_recent_samples_metadata" in x_df.columns
        ):
            current_date = str(x.split("/")[-1].split(".")[0].split("_")[-1])
            current_recent_submissions = x_df["number_recent_submissions"].values
            print(current_recent_submissions)
            if insert_loc > 1:
                previous_col_values = weekly_df.iloc[: insert_loc - 1].shift()
                print(previous_col_values)
                cumulative_recent_submissions_values = (
                    current_recent_submissions + previous_col_values.values
                )
                print(cumulative_recent_submissions_values)
                x_df_platforms = x_df["platform"].values
                to_add_recent_submissions_values = cumulative_recent_submissions_values
            else:
                to_add_recent_submissions_values = current_recent_submissions
            try:
                weekly_df.insert(
                    insert_loc, current_date, to_add_recent_submissions_values
                )
            except ValueError as exc:
                print(f"Lengths of values don't match: {exc}")
            insert_loc += 1

    weekly_df_tsv = weekly_df.to_csv(
        os.path.join(output_dir, "emergen_cluster_submissions.tsv")
    )
