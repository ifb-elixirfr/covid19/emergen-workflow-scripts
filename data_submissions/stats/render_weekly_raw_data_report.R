#!/usr/bin/env Rscript

library(markdown)

args = commandArgs(trailingOnly = TRUE)
rmd_file = args[1]
input = args[2]
output = args[3]
wd = args[4]

today = Sys.Date()

setwd(wd)
rmarkdown::render(rmd_file, output_file = output, params = list(input_file = input))
