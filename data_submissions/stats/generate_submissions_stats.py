"""

"""

from glob import glob
from pathlib import Path

import stat
import json
import os
import sys
import re
import datetime
import requests


def is_file_older_than(file, delta):
    """

    :param file:
    :param delta:
    :return:
    """
    cutoff = datetime.datetime.utcnow() - delta
    mtime = datetime.datetime.utcfromtimestamp(os.path.getmtime(file))
    if mtime < cutoff:
        return True
    return False


def is_writable(path):
    """

    :param path:
    :return:
    """
    try:
        os.access(path=os.path.abspath(path), mode=os.W_OK)
        if os.path.isdir(path):
            return True
        else:
            return False
    except Exception:
        return False


def is_readable(path):
    """

    :param path:
    :return:
    """
    try:
        os.access(path=os.path.abspath(path), mode=os.R_OK)
        if os.path.isdir(path):
            return True
        else:
            return False
    except Exception:
        return False


def get_platforms_map():
    """

    :return:
    """
    return {
        "ALPHABIO%20-%20Site Guinot": "alphabio",
        "CHU%20Martinique": "chu-martinique",
        "CNR%20VIR%20Pasteur": "institut-pasteur",
        "LABORATOIRE%20CERBA": "cerba",
        "CHU%20Saint-Etienne": "chu-saint-etienne",
        "CHU%20Strasbourg": "chu-strasbourg",
        "CHU%20Orléans": "chu-orleans",
        "CHU%20Rouen": "chu-rouen",
        "PIMIT-La%20Réunion": "pimit-la-reunion",
        "CHU%20Besançon": "chu-besancon",
        "CHU%20Grenoble": "chu-grenoble",
        "ESPACEBIO": "espacebio",
        "CHU%20Nantes": "chu-nantes",
        "CHU%20Angers": "chu-angers",
        "URBioscope": "urbioscope",
        "CHU%20Nice": "chu-nice",
        "CHU%20Guadeloupe": "chu-guadeloupe",
        "APHP%20Paul Brousse": "aphp-paul-brousse",
        "APHP%20Saint-Antoine": "aphp-saint-antoine",
        "CHU%20Tours": "chu-tours",
        "CHU%20Amiens": "chu-amiens",
        "Institut%20Pasteur%20Cayenne": "ip-cayenne",
        "CHU%20Nancy": "chu-nancy",
        "NOVELAB%20INGELS%20VIGNON": "novelab",
        "CH%20Toulon": "ch-toulon",
        "CHU%20Brest": "chu-brest",
        "CHU%20Toulouse": "chu-toulouse",
        "Hub%20bioinfo%20Pasteur": "hub-bioinfo-pasteur",
        "CHU%20Bordeaux": "chu-bordeaux",
        "LABORIZON%20CENTRE": "laborizon-centre",
        "CHU%20Nîmes": "chu-nimes",
        "LBM%20ALPIGENE": "alpigene",
        "CNR%20Virus%20des%20Infections%20Respiratoires%20-%20France%20SUD": "france-sud",
        "ORIAPOLE": "oriapole",
        "CHU%20Metz": "chu-metz",
        "IHU%20Méditerranée Infection": "ihu-marseille",
        "CHU%20Poitiers": "chu-poitiers",
        "APHP%20Avicenne": "aphp-avicenne",
        "Cellule%20d%27Intervention%20Biologique%20d%27Urgence": "urgence",
        "HIA%20Bégin": "hia-begin",
        "Faculté%20de%20Medecine%20URCA%20/%20CHU Reims": "fac_medecine_urca",
        "CHU%20Reims": "chu-reims",
        "IFB": "ifb",
        "IRBA%20Bretigny/Orge": "irba-bretigny-orge",
        "LxBIO": "lxbio",
        "Atos%20France": "atos-france",
        "modeller%20INRIA": "modeller-inra",
        "CHU%20Dijon": "chu-dijon",
        "CHU%20Rennes": "chu-rennes",
        "Atoutbio": "atoutbio",
        "CHU%20Caen": "chu-caen",
        "CHU%20Montpellier": "chu-montpellier",
        "EUROFINS%20BIOMNIS%20LYON": "eurofins-biomnis",
        "GEN-BIO%20INOVIE%20GRAVANCHES": "genbio-gravanches",
        "CHU%20Lille": "chu-lille",
        "SpF": "spf",
        "modeller%20INSERM": "modeller-inserm",
        "modeller%20Institut%20Pasteur": "modeller-pasteur",
        "Plateau%20technique%20de%20METZ%20VERRIERS": "pt-metz-verriers",
        "APHP%20Saint-Louis": "aphp-saint-louis",
        "HIA%20Percy": "hia-percy",
        "CH%20Bastia": "ch-bastia",
        "APHP%20Bichat-Claude%20Bernard": "aphp-bichat",
        "Biosoleil%20Guyane": "biosoleil-guyane",
        "CH%20Simone%20Veil": "ch-simone-veil",
        "CH%20Versailles": "ch-versailles",
        "CHU%20Clermont-Ferrand": "ch-clermont-ferrand",
        "APHP%20Cochin": "aphp-cochin",
        "Expert%20CNRS": "expert-cnrs",
        "modeller%20CNRS": "modeller-cnrs",
        "APHP%20Pitié-Salpêtrière": "aphp-pidie-salpetriere",
        "CHU%20Réunion": "chu-reunion",
        "APHP%20HEGP": "aphp-hegp",
        "CHU%20Limoges": "chu-limoges",
        "APHP-MONDOR": "aphp-mondor",
        "BIOPATH%20LABORATOIRES": "biopath",
    }


def get_emergendb_platform_name(platform_basename):
    """

    :param platform_basename:
    :return:
    """
    platforms_map = get_platforms_map()
    for k, v in platforms_map.items():
        if platform_basename == v:
            return k
    return False


def check_tags(x):
    """

    :param x:
    :return:
    """
    tags = ("downloaded", "clean", "running", "done", "final", "stuck")
    if x in tags:
        return False
    return True


def find_raw_data_files(submission_folder_to_process, emergendb_platform_name, platform_basename, token):
    """

    :param submission_folder_to_process:
    :param emergendb_platform_name:
    :param token:
    :param platform_basename:
    :return:
    """
    all_samples = []
    all_valid_samples = []
    all_errors = []
    has_samples = False

    print(f"\tProcessing submission folder: {submission_folder_to_process}")
    # bam
    all_bam_files = glob(f"{submission_folder_to_process}/**/*.bam*")
    all_bam_files_basename = [os.path.basename(x) for x in all_bam_files]
    for bam_file in all_bam_files_basename:
        sample_id = bam_file.replace(" ", "_")  # Replace all spaces in filename
        sample_id = re.sub(".bam(?:.gz)$", "", sample_id)  # Remove extension
        sample_id = re.sub(
            "_clean", "", sample_id
        )  # Remove clean suffix from decontaminated files
        sample_id = re.sub("^[0-9]{4}[-_](?:[0-9]{2}[-_][0-9]{2}[-_])?", "",
                           sample_id)  # Remove date prefix (old procedure)
        sample_id = re.sub(platform_basename, "", sample_id)  # Remove platform name (old procedure)
        sample_id = re.sub("^[-_]?", "", sample_id)
        sample_id = re.sub(
            "^semaine[0-9]{1,2}[-_]?", "", sample_id
        )  # Remove "semaine" prefix from filename
        sample_id = re.sub("[-_]semaine[0-9]{1,2}?", "", sample_id)  # Remove "semaine" string if present (old procedure)
        sample_id = re.sub(
            "[-_][Rr]?[0-9]*.$", "", sample_id
        )  # Remove R1/R2/channel suffix from filename
        if "_" in sample_id or "-" in sample_id:
            all_errors.append(sample_id)
        else:
            if sample_id not in all_samples:
                all_samples.append(sample_id)

    # fastq
    all_fastq_files = glob(f"{submission_folder_to_process}/**/*.f*q*")
    all_fastq_files_basename = [os.path.basename(x) for x in all_fastq_files]
    for fastq_file in all_fastq_files_basename:
        sample_id = fastq_file.replace(" ", "_")  # Replace all spaces in filename
        sample_id = re.sub(".f(?:ast)q(?:.gz)$", "", sample_id)  # Remove extension
        sample_id = re.sub(
            "_clean", "", sample_id
        )  # Remove clean suffix from decontaminated files
        sample_id = re.sub("^[0-9]{4}[-_](?:[0-9]{2}[-_][0-9]{2}[-_])?", "", sample_id)  # Remove date prefix
        sample_id = re.sub(platform_basename, "", sample_id)  # Remove platform name (old procedure)
        sample_id = re.sub("^[-_]?", "", sample_id)
        sample_id = re.sub("^semaine[0-9]{1,2}[-_]?", "", sample_id)  # Remove "semaine" prefix from filename
        sample_id = re.sub("[-_]semaine[0-9]{1,2}?", "", sample_id)  # Remove "semaine" string if present (old procedure)
        sample_id = re.sub("[-_][Rr]?[0-9]*.$", "", sample_id)  # Remove R1/R2/channel suffix from filename
        sample_id = re.sub("[-_]reads", "", sample_id)  # Remove "reads" suffix from filename
        complete_regex = re.compile(r"^semaine[0-9]{1,2}[-_]?[0-9]*[-_]?\w*[_-][Rr][12]?\.f(?:ast)q(?:.gz)")  # Complete regex match (add to test)

        if "_" in sample_id or "-" in sample_id:
            all_errors.append(sample_id)
        else:
            if sample_id not in all_samples:
                all_samples.append(sample_id)

    for sample_id in all_samples:
        resp = requests.get(
            f"https://emergen-db.france-bioinformatique.fr/fr/api/get/UID/by/{emergendb_platform_name}/{sample_id}/",
            headers={"Authorization": f"Token {token}"},
        )
        print(f"\t\tSending request {resp.url}")
        if resp.status_code == 200:
            all_valid_samples.append(sample_id)
    if len(all_bam_files) + len(all_fastq_files) > 0:
        has_samples = True

    return {
        "samples": all_samples,
        "valid_samples": all_valid_samples,
        "errors": all_errors,
        "has_samples": has_samples,
    }


def serialize_dict(dict_to_serialize: dict):
    """

    :param dict_to_serialize:
    :return:
    """
    return json.dumps(dict_to_serialize)


def get_ctime(file):
    """

    :param file:
    :return:
    """
    ctime = os.path.getctime(file)
    ctime_ts = datetime.datetime.fromtimestamp(ctime)

    return ctime_ts


def get_tree_size(path):
    """

    :param path:
    :return:
    """
    total_size = 0
    dirs = [path]
    while dirs:
        next_dir = dirs.pop()
        with os.scandir(next_dir) as it:
            for entry in it:
                if entry.is_dir(follow_symlinks=False):
                    dirs.append(entry.path)
                else:
                    total_size += entry.stat(follow_symlinks=False).st_size
    return total_size


def main(output_file, token):
    """

    :param output_file:
    :param token:
    :return:
    """
    projects_root_dir = "/shared/projects"
    emergen_projects = glob(f"{projects_root_dir}/emergen_*")

    all_submissions_count = 0
    all_valid_submissions_count = 0
    all_recent_submissions_count = 0
    all_samples_count = 0
    all_samples_with_metadata_count = 0
    all_recent_samples_count = 0
    all_recent_samples_metadata_count = 0
    all_metadata_files_count = 0

    platforms_map = get_platforms_map()

    now = datetime.datetime.now()
    today_as_date = datetime.datetime.strptime(now.strftime("%Y-%m-%d"), "%Y-%m-%d")

    with open(output_file, 'w', encoding='utf-8') as fhandle:
        fhandle.write(
            "platform"
            "\tdate"
            "\tnumber_submissions"
            "\tnumber_valid_submissions"
            "\tnumber_recent_submissions"
            "\tnumber_samples"
            "\tnumber_samples_metadata"
            "\tnumber_error_samples"
            "\tnumber_recent_samples"
            "\tnumber_recent_samples_metadata"
            "\tdisk_usage"
            "\tfirst_submission_date"
            "\tlast_submission_date"
            "\taverage_fastq_size"
            "\taverage_bam_size"
            "\n"
        )

        for platform_project in emergen_projects:
            if not is_readable(platform_project):  # Skip limited access projects prefixed with "emergen"
                continue
            platform_samples_count = 0
            platform_samples_with_metadata_count = 0
            platform_samples_errors = 0
            platform_submissions_count = 0
            platform_valid_submissions_count = 0
            platform_error_submissions = 0
            platform_recent_submissions_count = 0  # Last X days
            platform_recent_samples_count = 0
            platform_recent_samples_with_metadata_count = 0

            platform_basename = os.path.basename(platform_project).split("_")[-1]
            emergendb_platform_name = get_emergendb_platform_name(platform_basename=platform_basename)
            # Folder needs to match this regex to be considered a submission
            submission_regex = re.compile(
                r"^.*\/([0-9]{4}[_-]?[0-9]{2}[_-]?[0-9]{2}).*"
            )

            if platform_project not in {"emergen_anrs-mie_", "emergen_cnr_", "emergen_private"}:
                try:
                    project_disk_usage = get_tree_size(platform_project)
                    all_submissions_folders_unvalidated = [
                        os.path.abspath(d) for d in os.listdir(platform_project)
                    ]
                    all_submissions_folders = []

                    submission_folders_size = 0
                    is_submission_recent = False
                    first_valid_submission_date = today_as_date + datetime.timedelta(days=1)
                    last_valid_submission_date = today_as_date - datetime.timedelta(
                        days=1461
                    )
                    bam_files_mean_size = "NA"
                    fastq_files_mean_size = "NA"

                    for folder in all_submissions_folders_unvalidated:
                        if (
                            is_writable(
                                os.path.abspath(
                                    f"{platform_project}/{os.path.basename(folder)}"
                                )
                            )
                            and "AMI" not in folder
                            and "ami" not in folder
                        ):
                            all_submissions_folders.append(
                                os.path.abspath(
                                    os.path.abspath(
                                        f"{platform_project}/{os.path.basename(folder)}"
                                    )
                                )
                            )
                        else:
                            continue

                    # Parse list of found folders in project and match against regex to verify validity
                    # print(all_submissions_folders)
                    for submission_folder in all_submissions_folders:
                        submission_date = re.findall(submission_regex, submission_folder)
                        if submission_date:  # Submission folder must follow naming
                            submission_date_to_convert = submission_date[0].replace("_", "-")
                            try:
                                submission_date_as_date = datetime.datetime.strptime(
                                    submission_date_to_convert, "%Y-%m-%d"
                                )
                            except ValueError:
                                print(f"Date error in submission folder {submission_folder} for platform {platform_basename}")
                                continue
                            # Determine wether submission is recent or not
                            if submission_date_as_date < first_valid_submission_date:
                                first_valid_submission_date = submission_date_as_date
                            if submission_date_as_date > last_valid_submission_date:
                                last_valid_submission_date = submission_date_as_date
                            days_delta = today_as_date - submission_date_as_date
                            submission_days_delta = days_delta - datetime.timedelta(days=7)
                            if submission_days_delta <= datetime.timedelta(days=7):
                                is_submission_recent = True
                                platform_recent_submissions_count += 1

                            # Samples counting
                            submission_raw_data = find_raw_data_files(
                                submission_folder_to_process=os.path.abspath(submission_folder),
                                emergendb_platform_name=emergendb_platform_name,
                                token=token,
                                platform_basename=platform_basename
                            )
                            submission_samples = submission_raw_data["samples"]
                            submission_valid_samples = submission_raw_data["valid_samples"]
                            submission_errors = submission_raw_data["errors"]
                            submission_has_samples = submission_raw_data["has_samples"]
                            platform_submissions_count += 1

                            # Get size of folder's content
                            submission_folders_size = submission_folders_size + get_tree_size(submission_folder)
                            submission_folder_path = os.path.join(
                                os.path.abspath(platform_project), submission_folder
                            )

                            platform_samples_count += len(submission_samples)
                            if len(submission_errors) > 0:
                                platform_error_submissions += 1
                                platform_samples_errors += len(submission_errors)
                            if len(submission_valid_samples) > 0:
                                platform_valid_submissions_count += 1
                                platform_samples_with_metadata_count += len(submission_valid_samples)
                            if is_submission_recent:
                                platform_recent_samples_count += len(submission_samples)
                                platform_recent_samples_with_metadata_count += len(
                                    submission_valid_samples
                                )
                        else:
                            continue

                    # Add to total counts
                    all_submissions_count += platform_submissions_count
                    all_valid_submissions_count += platform_valid_submissions_count
                    all_recent_submissions_count += platform_recent_submissions_count
                    all_samples_count += platform_samples_count
                    all_recent_samples_count += platform_recent_samples_count
                    all_recent_samples_metadata_count += (
                        platform_recent_samples_with_metadata_count
                    )

                    # Check there is a first submission date
                    if first_valid_submission_date == today_as_date + datetime.timedelta(
                        days=1
                    ):
                        first_valid_submission_date = "NA"
                    if last_valid_submission_date == today_as_date - datetime.timedelta(
                        days=1461
                    ):
                        last_valid_submission_date = "NA"

                    # Print out projects results
                    print(f"Platform: {os.path.basename(platform_project)}")
                    print(f"\tNumber of submissions: {platform_submissions_count}")
                    print(
                        f"\tNumber of valid submissions: {platform_valid_submissions_count}"
                    )
                    print(f"\tNumber of submissions with at least one error: {platform_error_submissions}")
                    print(
                        f"\tNumber of recent submissions: {platform_recent_submissions_count}"
                    )
                    print(f"\tNumber of samples: {platform_samples_count}")
                    print(
                        f"\tNumber of samples with metadata: {platform_samples_with_metadata_count}"
                    )
                    print(
                        f"\tNumber of error (parsing samples): {platform_samples_errors}"
                    )
                    print(f"\tNumber of recent samples: {platform_recent_samples_count}")
                    print(
                        f"\tNumber of recent samples with metadata: {platform_recent_samples_with_metadata_count}"
                    )
                    print(f"\tProject disk usage: {project_disk_usage}")
                    print(
                        f"\tFirst valid submission date: {str(first_valid_submission_date).split(' ', maxsplit=1)[0]}"
                    )
                    print(
                        f"\tLast valid submission date: {str(last_valid_submission_date).split(' ', maxsplit=1)[0]}"
                    )

                    # Write to tabular stats file
                    fhandle.write(
                        f"{os.path.basename(platform_project)}"
                        f"\t{today_as_date}"
                        f"\t{platform_submissions_count}"
                        f"\t{platform_valid_submissions_count}"
                        f"\t{platform_recent_submissions_count}"
                        f"\t{platform_samples_count}"
                        f"\t{platform_samples_with_metadata_count}"
                        f"\t{platform_samples_errors}"
                        f"\t{platform_recent_samples_count}"
                        f"\t{platform_recent_samples_with_metadata_count}"
                        f"\t{submission_folders_size}"
                        f"\t{str(first_valid_submission_date).split(' ', maxsplit=1)[0]}"
                        f"\t{str(last_valid_submission_date).split(' ', maxsplit=1)[0]}"
                        f"\t{str(fastq_files_mean_size)}"
                        f"\t{str(bam_files_mean_size)}"
                        f"\n"
                    )

                except PermissionError:
                    print(f"Insufficient permissions to access {platform_project}")
                    continue
            else:
                print(f"Project {platform_project} is not a platform")

    # Print out global results
    print("----------------------------")
    print(f"Total number of submissions: {all_submissions_count}")
    print(f"Total number of valid submissions: {all_valid_submissions_count}")
    print(f"Total number of recent submissions: {all_recent_submissions_count}")
    print(f"Total number of samples: {all_samples_count}")
    print(f"Total number of metadata files: {all_metadata_files_count}")
    print(f"Total number of samples with metadata: {all_samples_with_metadata_count}")
    print(f"Total number of recent samples: {all_recent_samples_count}")
    print(
        f"Total number of recent samples with metadata: {all_recent_samples_metadata_count}"
    )

    # Set group rights to read for report
    os.chmod(os.path.abspath(output_file), stat.S_IXGRP)

    return str(os.path.abspath(output_file))


if __name__ == "__main__":
    today = datetime.date.today()
    ftoday = today.strftime("%Y%m%d")
    if sys.argv[1] and sys.argv[2]:
        outfile = os.path.join(
            os.path.abspath(sys.argv[1]), f"raw_data_submissions_{ftoday}.tsv"
        )
        print(f"Output file: {outfile}")
        main(output_file=outfile, token=str(sys.argv[2]))
    else:
        print("No output file detected, please specify one as script argument")
        sys.exit()
