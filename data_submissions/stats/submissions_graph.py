#!/usr/bin/python3
import os
import pwd
import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime


def main(submission_stats):
    user = pwd.getpwuid(os.getuid())[0]
    os.makedirs(
        f"/home/{user}/emergen-workflow-plots", exist_ok=True
    )  # Save plots here

    today_strf = datetime.datetime.today().strftime("%Y%m%d")
    today_strp = datetime.datetime.today().strptime(today_strf, "%Y%m%d")
    day = today_strp.day
    month = today_strp.month
    year = today_strp.year
    week_number = datetime.date(year, month, day).strftime("%V")

    df = pd.read_csv(submission_stats, header=0, sep="\t")

    # TOTAL SUBMISSIONS

    sub_df_submissions = df[df["number_submissions"] != 0]

    y_pos = np.arange(len(sub_df_submissions["platform"]))
    width = 0.3
    padding = 0.3

    fig, ax = plt.subplots()

    rects1 = ax.barh(
        y_pos - padding,
        sub_df_submissions["number_submissions"],
        width,
        label="Total submissions",
        align="center",
    )
    rects2 = ax.barh(
        y_pos,
        sub_df_submissions["number_valid_submissions"],
        width,
        label="Valid submissions (upload procedure respected)",
        align="center",
    )
    rects3 = ax.barh(
        y_pos + padding,
        sub_df_submissions["number_recent_submissions"],
        width,
        label="Recent submissions (< 7 days)",
        align="center",
    )

    ax.set_xlabel("Number of submissions")
    ax.set_title("Number of submissions per active platform")
    ax.set_yticks(y_pos, sub_df_submissions["platform"])
    ax.invert_yaxis()
    ax.legend()
    ax.bar_label(rects1, padding=3)
    ax.bar_label(rects2, padding=3)
    ax.bar_label(rects3, padding=3)

    plt.rcdefaults()
    fig.tight_layout()
    figure = plt.gcf()
    figure.set_size_inches(16, 12)

    if os.path.exists(f"/home/{user}/emergen-workflow-plots/total_submissions.png"):
        os.remove(f"/home/{user}/emergen-workflow-plots/total_submissions.png")

    plt.savefig(f"/home/{user}/emergen-workflow-plots/total_submissions.png")

    # RECENT SUBMISSIONS

    sub_df_submissions = df[df["number_recent_submissions"] != 0]

    y_pos = np.arange(len(sub_df_submissions["platform"]))
    width = 0.3
    padding = 0.3

    fig, ax = plt.subplots()

    rects = ax.barh(
        y_pos - padding,
        sub_df_submissions["number_recent_submissions"],
        width,
        align="center",
    )

    ax.set_xlabel("Number of recent submissions (< 7 days)")
    ax.set_title("Number of recent submissions (< 7 days) per active platform")
    ax.set_yticks(y_pos, sub_df_submissions["platform"])
    ax.invert_yaxis()
    ax.legend()
    ax.bar_label(rects, padding=1)

    plt.rcdefaults()
    fig.tight_layout()
    figure = plt.gcf()
    figure.set_size_inches(16, 12)

    if os.path.exists(f"/home/{user}/emergen-workflow-plots/recent_submissions.png"):
        os.remove(f"/home/{user}/emergen-workflow-plots/recent_submissions.png")

    plt.savefig(f"/home/{user}/emergen-workflow-plots/recent_submissions.png")

    # DISK USAGE


if __name__ == "__main__":
    sub_stats_file = sys.argv[1]
    main(submission_stats=sub_stats_file)
