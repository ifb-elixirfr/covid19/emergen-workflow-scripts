import os
import glob
import sys
import logging

from datetime import datetime

# Create a logger and set minimal level to debug
logger = logging.getLogger("emergen-data-submission-conformity")
logger.setLevel(logging.DEBUG)

# Create a console handler and set minimal level to debug
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# Format for the logging message
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# Set the format for the console handler
console_handler.setFormatter(formatter)
# Affect console handler to the logger
logger.addHandler(console_handler)
