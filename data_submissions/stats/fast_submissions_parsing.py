#!/usr/bin/python3

import os
import re
import sys
import datetime

import plotly.express as px
import pandas as pd

from glob import glob


def is_writable(path):
    """

    :param path:
    :return:
    """
    try:
        os.access(path=os.path.abspath(path), mode=os.W_OK)
        if os.path.isdir(path):
            return True
        else:
            return False
    except Exception:
        return False


def is_readable(path):
    """

    :param path:
    :return:
    """
    try:
        os.access(path=os.path.abspath(path), mode=os.R_OK)
        if os.path.isdir(path):
            return True
        else:
            return False
    except Exception:
        return False


def dict_values_to_list(dict_to_transform):
    """

    :param dict_to_transform:
    :return:
    """

    ret_list = []
    for v in dict_to_transform.values():
        ret_list.append(str(v))

    return ret_list


def find_raw_data_files(submission_folder_to_process, platform_basename):
    """

    :param submission_folder_to_process:
    :param platform_basename:
    :return:
    """
    all_samples = []
    has_samples = False

    print(f"\tProcessing submission folder: {submission_folder_to_process}")
    # bam
    all_bam_files = glob(f"{submission_folder_to_process}/**/*.bam*")
    all_bam_files_basename = [os.path.basename(x) for x in all_bam_files]
    for bam_file in all_bam_files_basename:
        sample_id = bam_file.replace(" ", "_")  # Replace all spaces in filename
        sample_id = re.sub(".bam(?:.gz)$", "", sample_id)  # Remove extension
        sample_id = re.sub(
            "_clean", "", sample_id
        )  # Remove clean suffix from decontaminated files
        sample_id = re.sub("^[0-9]{4}[-_](?:[0-9]{2}[-_][0-9]{2}[-_])?", "",
                           sample_id)  # Remove date prefix (old procedure)
        sample_id = re.sub(platform_basename, "", sample_id)  # Remove platform name (old procedure)
        sample_id = re.sub("^[-_]?", "", sample_id)
        sample_id = re.sub(
            "^semaine[0-9]{1,2}[-_]?", "", sample_id
        )  # Remove "semaine" prefix from filename
        sample_id = re.sub("[-_]semaine[0-9]{1,2}?", "", sample_id)  # Remove "semaine" string if present (old procedure)
        sample_id = re.sub(
            "[-_][Rr]?[0-9]*.$", "", sample_id
        )  # Remove R1/R2/channel suffix from filename
        if sample_id not in all_samples:
            all_samples.append(sample_id)

    # fastq
    all_fastq_files = glob(f"{submission_folder_to_process}/**/*.f*q*")
    all_fastq_files_basename = [os.path.basename(x) for x in all_fastq_files]
    for fastq_file in all_fastq_files_basename:
        sample_id = fastq_file.replace(" ", "_")  # Replace all spaces in filename
        sample_id = re.sub(".f(?:ast)q(?:.gz)$", "", sample_id)  # Remove extension
        sample_id = re.sub(
            "_clean", "", sample_id
        )  # Remove clean suffix from decontaminated files
        sample_id = re.sub("^[0-9]{4}[-_](?:[0-9]{2}[-_][0-9]{2}[-_])?", "", sample_id)  # Remove date prefix
        sample_id = re.sub(platform_basename, "", sample_id)  # Remove platform name (old procedure)
        sample_id = re.sub("^[-_]?", "", sample_id)
        sample_id = re.sub("^semaine[0-9]{1,2}[-_]?", "", sample_id)  # Remove "semaine" prefix from filename
        sample_id = re.sub("[-_]semaine[0-9]{1,2}?", "", sample_id)  # Remove "semaine" string if present (old procedure)
        sample_id = re.sub("[-_][Rr]?[0-9]*.$", "", sample_id)  # Remove R1/R2/channel suffix from filename
        sample_id = re.sub("[-_]reads", "", sample_id)  # Remove "reads" suffix from filename
        complete_regex = re.compile(r"^semaine[0-9]{1,2}[-_]?[0-9]*[-_]?\w*[_-][Rr][12]?\.f(?:ast)q(?:.gz)")  # Complete regex match (add to test)

        if sample_id not in all_samples:
            all_samples.append(sample_id)

    if len(all_bam_files) + len(all_fastq_files) > 0:
        has_samples = True

    return {
        "samples": all_samples,
        "has_samples": has_samples,
    }


def main(csv_file, html_file):
    """
    :param csv_file:
    :return:
    """
    projects_root_dir = "/shared/projects"
    emergen_projects = glob(f"{projects_root_dir}/emergen_*")

    submissions_timeline_dict = {}
    submission_regex = re.compile(r"^.*\/([0-9]{4}[_-]?[0-9]{2}[_-]?[0-9]{2}).*")
    platform_regex = re.compile(r".*emergen_(anrs-mie|cnr|private)_.*$")

    current_ym = datetime.datetime.today().strftime("%Y%m")

    header_timeseries_list = ["platform"]
    for y in range(2020, 2024):
        for m in range(1, 13):
            if m > 9:
                m_str = str(m)
            else:
                m_str = f"0{m}"
            if int(f"{y}{m_str}") > int(current_ym):
                break
            if y == 2020 and m < 9:
                continue
            ym = f"{str(y)}-{m_str}"
            header_timeseries_list.append(ym)

    with open(csv_file, "w", encoding="utf-8") as fhandle:
        fhandle.write(",".join(header_timeseries_list))
        fhandle.write("\n")
        for platform_project in emergen_projects:

            if not is_readable(platform_project):
                continue

            if re.match(platform_regex, platform_project):

                print(f"Platform project {os.path.basename(platform_project)}")
                platform_basename = os.path.basename(platform_project).split("_")[-1]

                platform_dict = {"platform": platform_basename}

                for y in range(2020, 2024):
                    for m in range(1, 13):
                        if m > 9:
                            m_str = str(m)
                        else:
                            m_str = f"0{m}"
                        if int(f"{y}{m_str}") > int(current_ym):
                            break
                        if y == 2020 and m < 9:
                            continue
                        ym = f"{str(y)}-{m_str}"
                        platform_dict[ym] = 0

                if platform_basename not in submissions_timeline_dict.keys():
                    submissions_timeline_dict[platform_basename] = {}

                all_submissions_folders = []

                try:
                    all_submissions_folders_unvalidated = [
                        os.path.abspath(d) for d in os.listdir(platform_project)
                    ]
                except PermissionError:
                    continue

                for folder in all_submissions_folders_unvalidated:
                    if "ami" not in folder.lower():
                        all_submissions_folders.append(
                            os.path.abspath(
                                os.path.abspath(
                                    f"{platform_project}/{os.path.basename(folder)}"
                                )
                            )
                        )
                    else:
                        continue

                for submission_folder in all_submissions_folders:
                    submission_date = re.findall(submission_regex, submission_folder)
                    if submission_date and len(submission_date) == 1:
                        submission_date_to_convert = submission_date[0].replace("_", "-")
                        try:
                            submission_date_as_date = datetime.datetime.strptime(
                                submission_date_to_convert, "%Y-%m-%d"
                            )
                            submission_date_split = str(submission_date_as_date).split('-')
                            submission_year = submission_date_split[0]
                            submission_month = submission_date_split[1]
                            submission_ym = f"{submission_year}-{submission_month}"
                            raw_data_files = find_raw_data_files(submission_folder_to_process=submission_folder,
                                                                 platform_basename=os.path.basename(platform_project).split("_")[-1])
                            num_samples = len(raw_data_files["samples"])
                            platform_dict[submission_ym] += num_samples
                        except ValueError:
                            print(
                                f"Date error in submission folder {submission_folder} for platform {platform_basename}")
                            continue

                platform_time_series_list = dict_values_to_list(dict_to_transform=platform_dict)
                fhandle.write(",".join(platform_time_series_list))
                fhandle.write("\n")

    create_plots(csv_file=csv_file, html_file=html_file)


def figures_to_html(plots, html_file):
    """

    :param plots:
    :param html_file:
    :return:
    """

    with open(html_file, 'w') as dashboard:
        dashboard.write("<html><head></head><body>" + "\n")
        for plot in plots:
            inner_html = plot.to_html().split('<body>')[1].split('</body>')[0]
            dashboard.write(inner_html)
        dashboard.write("</body></html>" + "\n")

        return dashboard


def create_plots(csv_file, html_file):
    """

    :param csv_file:
    :param html_file:
    :return:
    """

    df = pd.read_csv(csv_file, sep=",", header=None)
    dft = df.T  # Transpose dataframe to be able to manipulate data more easily
    dft.columns = dft.iloc[0]  # Set header
    dft = dft[1:]
    time_series = dft.iloc[:, 0]  # Plot axis ticks

    all_plots = []

    for name, values in dft.iteritems():
        if name == "platform":
            continue  # Skip first column
        values_num = pd.to_numeric(values)
        curr_plot = px.bar(x=time_series, y=values_num, title=f"Number of unique samples uploaded for {name}")
        curr_plot.update_traces(textangle=0, textposition="outside", cliponaxis=False)
        curr_plot.update_layout(
            xaxis_title="Month",
            yaxis_title="Samples",
            yaxis_range=[0, max(values_num)],
            yaxis=dict(
                tickmode="linear",
            ),
            xaxis=dict(
                tickmode='array',  # change 1
                tickvals=time_series,  # change 2
                ticktext=time_series,  # change 3
            ),
        )
        all_plots.append(curr_plot)

    figures_to_html(plots=all_plots, html_file=html_file)

    return all_plots


if __name__ == "__main__":
    csv = os.path.abspath(sys.argv[1])
    html = os.path.abspath(sys.argv[2])
    main(csv_file=csv, html_file=html)
